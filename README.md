# cordova-plugin-mock-location-helper

This is a cordova plugin to check:
  
  1) Development mode is enabled\disabled.
  2) Mock Location Data is enabled\disabled 

in android settings.

## Supported Platforms

- Android

## Installation

    ionic cordova plugin add https://acilimbilgisayar@bitbucket.org/acilimbilgisayar/cordova-plugin-mocklocation.git

## Update

  For update its recomended to remove and add plugin again;

  ionic cordova plugin rm cordova-plugin-mock-location-helper

  ionic cordova plugin add https://acilimbilgisayar@bitbucket.org/acilimbilgisayar/cordova-plugin-mocklocation.git
  

## Usage

Available methods

```ts

isDevelopmentModeEnabled(successCallback, errorCallback);

isMockLocationEnabled(successCallback, errorCallback);

```

Sample usage

You can create a helper function like below: 

```ts

checkDevelopmentModeEnabled(): Promise<boolean>{

    let developmentModePromise = new Promise<boolean>((resolve, reject) => {
      window['plugins'].mocklocationhelper.isDevelopmentModeEnabled(res => resolve(res), error => reject(error));
    });

    return developmentModePromise.then(result => {
      return result;
    });

  }
```

  Then you can use

```ts

  checkDevelopmentModeEnabled().then((enabled: boolean) => {
    if (enabled) {
      //
    }
  })

```


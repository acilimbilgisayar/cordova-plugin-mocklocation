var mocklocation = {
  isMockLocationEnabled: function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, 'MockLocation', 'isMockLocationEnabled', []);
  },
  isDevelopmentModeEnabled: function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, 'MockLocation', 'isDevelopmentModeEnabled', []);
  },
  isThereAnyFakeLocationApp: function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, 'MockLocation', 'isThereAnyFakeLocationApp', []);
  },
  mockPermissionedApps: function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, 'MockLocation', 'mockPermissionedApps', []);
  }
}

cordova.addConstructor(function () {
  if (!window.plugins) {window.plugins = {};}
  
  window.plugins.mocklocationhelper = mocklocation;
  return window.plugins.mocklocationhelper;
});

package com.acilim.threes.mocklocationhelper;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MockLocation extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {
        if (action.equals("isMockLocationEnabled")) {
            callbackContext.success(isMockLocationEnabled() ? 1 : 0);
            return true;
        } else if (action.equals("isDevelopmentModeEnabled")) {
            callbackContext.success(isDevelopmentModeEnabled() ? 1 : 0);
            return true;
        } else if (action.equals("isThereAnyFakeLocationApp")) {
            callbackContext.success(isThereAnyFakeLocationApp() ? 1 : 0);
            return true;
        } else if (action.equals("mockPermissionedApps")) {
            callbackContext.success(isThereAnyFakeLocationApp() ? mockPermissionedApps().toString() : null);
            return true;
        } else {
            return false;
        }
    }

    /**
     * For Build.VERSION.SDK_INT < 18 i.e. JELLY_BEAN_MR2
     * Check if MockLocation setting is enabled or not
     *
     */
    private boolean isMockLocationEnabled(){
        return !Secure.getString(this.cordova.getActivity().getContentResolver(), Secure.ALLOW_MOCK_LOCATION).equals("0");
    }

    private boolean isDevelopmentModeEnabled(){
        return Secure.getString(this.cordova.getActivity().getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED).equals("1");
    }

    private boolean isThereAnyFakeLocationApp(){
        return mockPermissionedApps().size() > 0;
    }

    private List<String> mockPermissionedApps() {

        List<String> mockPermissionedApps = new ArrayList<String>();

        Context context = this.cordova.getActivity();
        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages =
                pm.getInstalledApplications(PackageManager.GET_META_DATA);

        for (ApplicationInfo applicationInfo : packages) {
            try {
                PackageInfo packageInfo = pm.getPackageInfo(applicationInfo.packageName, PackageManager.GET_PERMISSIONS);

                // Get Permissions
                String[] requestedPermissions = packageInfo.requestedPermissions;

                if (requestedPermissions != null) {
                    for (int i = 0; i < requestedPermissions.length; i++) {
                        if (requestedPermissions[i]
                                .equals("android.permission.ACCESS_MOCK_LOCATION")
                                && !applicationInfo.packageName.equals(context.getPackageName())
                                && !isSystemPackage(context, applicationInfo.packageName)
                                && !applicationInfo.packageName.equals("com.acilim.threes.merch.plus")
                                && !applicationInfo.packageName.equals("com.acilim.threes.merch")
                                && !applicationInfo.packageName.equals("ThreeS")
                                && !applicationInfo.packageName.equals("3S Merch Plus")) {
                            mockPermissionedApps.add(applicationInfo.packageName);
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("Got exception " , e.getMessage());
            }
        }

        return mockPermissionedApps;
    }

    private boolean isSystemPackage(Context context, String app){
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo pkgInfo = packageManager.getPackageInfo(app, 0);
            return (pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

}
